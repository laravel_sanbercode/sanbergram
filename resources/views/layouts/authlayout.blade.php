<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Title --}}
    <title>{{ config('app.name', 'Sanbergram') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
</head>
<body>
    <div class="limiter rounded-top">
		<div class="container-login100">
			<div class="wrap-login100 rounded">
				@yield('form')
				<div id="carouselExampleIndicators" class="carousel slide login100-more" data-ride="carousel">
			
			<div class="carousel-inner">
				<div class="carousel-item active">
				<img src="https://source.unsplash.com/collection/1274885/1000x1150" class="d-block w-100" alt="Slider">
				</div>
				<div class="carousel-item">
				<img src="https://source.unsplash.com/collection/458030/1000x1150" class="d-block w-100" alt="Slider">
				</div>
				<div class="carousel-item">
				<img src="https://source.unsplash.com/collection/291441/1000x1150" class="d-block w-100" alt="Slider">
				</div>
				<div class="carousel-item">
				<img src="https://source.unsplash.com/collection/1728085/1000x1150" class="d-block w-100" alt="Slider">
				</div>
				<div class="carousel-item">
				<img src="https://source.unsplash.com/collection/9556132/1000x1150" class="d-block w-100" alt="Slider">
				</div>
			</div>
			
			</div>
				
			</div>
		</div>
	</div>

	<script>

		document.querySelector('.login100-form').addEventListener('submit', function(e){
			var error = document.querySelector('.is-invalid');
			if(error) hideLoading();
			showLoading();
		})

		function showLoading() {
			document.querySelector('.spinner-border').classList.remove('d-none')
		}

		function hideLoading() {
			document.querySelector('.spinner-border').classList.add('d-none')
		}

	</script>
</body>
</html>
